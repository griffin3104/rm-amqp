import amqp from "amqplib";

/**
 * Exchange type 列挙型
 */
export enum ExchangeType {
  fanout = "fanout",
  direct = "direct",
  topic = "topic"
}

/**
 * AMQP接続設定型
 */
export interface AmqpConnectConfig {
  host: string;
  port: number;
  user: string;
  password: string;
  vhost: string;
}

/**
 * AMQP送信メッセージ（文字列）型
 */
export interface AmqpSendMsg {
  msg: string;
  bindKey: string;
}

/**
 * AMQP送信メッセージ（バイナリ）型
 */
export interface AmqpSendData {
  data: Buffer;
  bindKey: string;
}

/**
 * AMQP 基底クラス
 * AMQPへ接続するクラスの基底クラス
 * ----------------------------------------------------------------------------
 * @author LIS
 */
export class RmAmqpBase {
  uri = "";
  connected = false;
  conn: amqp.Connection = null;

  /**
   * コンストラクタ
   * @param conf 接続設定
   */
  constructor(conf: AmqpConnectConfig) {
    this.uri = `amqp://${conf.user}:${conf.password}@${conf.host}:${conf.port}`;
  }

  /**
   * 接続処理
   */
  async connect(): Promise<amqp.Connection> {
    if (this.connected == false) {
      this.conn = await amqp.connect(this.uri);
      this.connected = true;
    }
    return Promise.resolve(this.conn);
  }

  /**
   * チャネル作成
   */
  async createChannel(): Promise<amqp.Channel> {
    if (this.connected == false) {
      throw "接続されていないので、チャネルは作成できません。";
    }
    const ch = await this.conn.createChannel();
    return Promise.resolve(ch);
  }

  /**
   * 切断処理
   * ５秒後に切断する
   */
  close(): void {
    if (!this.connected) return;
    setTimeout(
      conn => {
        conn.close();
        this.connected = false;
      },
      500,
      this.conn
    );
  }
}

/**
 * AMQP Producerクラス
 * AMQPのProduserを生成し、メッセージ送信を可能とする。
 * ----------------------------------------------------------------------------
 * @author LIS
 */
export class RmAmqpProducer extends RmAmqpBase {
  /**
   * コンストラクタ
   * @param conf 接続設定
   */
  constructor(conf: AmqpConnectConfig) {
    super(conf);
  }

  /**
   * AMQPメッセージ送信（文字列メッセージ）
   * @param ch Channel
   * @param exName Exchange名
   * @param exType Exchangeタイプ
   * @param msg 送信メッセージ
   */
  async publishMessage(
    ch: amqp.Channel,
    exName: string,
    exType: ExchangeType,
    msg: AmqpSendMsg
  ): Promise<amqp.Channel> {
    const datas: AmqpSendData[] = [];
    const data: AmqpSendData = {
      data: Buffer.from(msg.msg),
      bindKey: msg.bindKey
    };
    datas.push(data);

    return this.publishBinarys(ch, exName, exType, datas);
  }

  /**
   * AMQP複数メッセージ送信（文字列メッセージ）
   * @param ch Channel
   * @param exName Exchange名
   * @param exType Exchangeタイプ
   * @param msgs 送信メッセージ配列
   */
  async publishMessages(
    ch: amqp.Channel,
    exName: string,
    exType: ExchangeType,
    msgs: AmqpSendMsg[]
  ): Promise<amqp.Channel> {
    const datas: AmqpSendData[] = [];
    msgs.forEach(msg => {
      const data: AmqpSendData = {
        data: Buffer.from(msg.msg),
        bindKey: msg.bindKey
      };
      datas.push(data);
    });

    return this.publishBinarys(ch, exName, exType, datas);
  }

  /**
   * AMQPデータ送信（バイナリメッセージ）
   * @param ch Channel
   * @param exName Exchange名
   * @param exType Exchangeタイプ
   * @param data 送信データ
   */
  async publishBinary(
    ch: amqp.Channel,
    exName: string,
    exType: ExchangeType,
    data: AmqpSendData
  ): Promise<amqp.Channel> {
    const datas: AmqpSendData[] = [];
    datas.push(data);

    return this.publishBinarys(ch, exName, exType, datas);
  }

  /**
   * AMQP複数データ送信（バイナリメッセージ）
   * @param ch Channel
   * @param exName Exchange名
   * @param exType Exchangeタイプ
   * @param datas 送信データ配列
   */
  async publishBinarys(
    ch: amqp.Channel,
    exName: string,
    exType: ExchangeType,
    datas: AmqpSendData[]
  ): Promise<amqp.Channel> {
    //Exchange差k末井
    ch.assertExchange(exName, exType.toString(), { durable: true });
    //Exchangeにバインドキーを指定たメッセージを送信させる
    for (const data of datas) {
      ch.publish(exName, data.bindKey, data.data);
    }
    return Promise.resolve(ch);
  }
}

/**
 * AMQP Consumerクラス
 * AMQPのConsumerを生成し、メッセージ送信を可能とする。
 * ----------------------------------------------------------------------------
 * @author LIS
 */
export class RmAmqpConsumer extends RmAmqpBase {
  /**
   * コンストラクタ
   * @param conf 接続設定
   */
  constructor(conf: AmqpConnectConfig) {
    super(conf);
  }

  /**
   * コンシューマ実行
   * @param exName Exchange名
   * @param exType Exchengeタイプ
   * @param queueName キュー名
   * @param bindKeys Exchange and Queueバインドキー
   * @param func メッセージ受信処理
   */
  async consume(
    ch: amqp.Channel,
    exName: string,
    exType: ExchangeType,
    queueName: string,
    bindKeys: string[],
    // eslint-disable-next-line @typescript-eslint/no-explicit-any
    consumeFunc: (msg: amqp.ConsumeMessage) => any
  ): Promise<amqp.Channel> {
    //Exchange作成
    ch.assertExchange(exName, exType.toString(), { durable: true });
    //受信Queue作成
    const q = await ch.assertQueue(queueName, { exclusive: true });
    //ExchangeとQueueの関係を設定
    bindKeys.forEach(key => ch.bindQueue(q.queue, exName, key));
    //consumeオプション生成
    const conOption: amqp.Options.Consume = {
      noAck: false
    };
    //受信処理登録
    ch.consume(q.queue, consumeFunc, conOption);
    return Promise.resolve(ch);
  }
}
