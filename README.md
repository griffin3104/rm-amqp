# Node.js用amqpクラスライブラリ

内部で、amqplibを使用。

## producer

```javascript
import * as AMQP from "./rm-amqp";

async function main(): Promise<void> {
  const producer = new AMQP.RmAmqpProducer({
    host: "192.168.56.108",
    port: 5672,
    user: "lis",
    password: "lis0101",
    vhost: "/",
  });

  try {
    await producer.connect();
    const ch = await producer.createChannel(
      "exName_hoge",
      AMQP.ExchangeType.direct
    );
    const msg = JSON.stringify({ hoge: "hogehoge" });
    ch.publishSimpleMessage(msg, "v1");
  } catch (err) {
    console.log(err);
  }

  const ret = await producer.close();
  process.exit(0);

  return;
}

main();
```

## consumer

```javascript
import * as AMQP from "./rm-amqp";

async function main(): Promise<void> {
  const consumer = new AMQP.RmAmqpConsumer({
    host: "192.168.56.108",
    port: 5672,
    user: "lis",
    password: "lis0101",
    vhost: "/",
  });
  try {
    await consumer.connect();
    const ch = await consumer.createChannel();

    await consumer.consume(
      ch,
      "exName_hoge",
      AMQP.ExchangeType.direct,
      "amqpTestQueue",
      ["v1"],
      (msg) => {
        console.log("Get AMQP Message:", msg.content.toString());
        ch.ack(msg, msg);
      }
    );
  } catch (err) {
    console.log(err);
  }
}

main();
```

