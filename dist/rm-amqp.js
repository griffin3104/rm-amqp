"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
var __values = (this && this.__values) || function(o) {
    var s = typeof Symbol === "function" && Symbol.iterator, m = s && o[s], i = 0;
    if (m) return m.call(o);
    if (o && typeof o.length === "number") return {
        next: function () {
            if (o && i >= o.length) o = void 0;
            return { value: o && o[i++], done: !o };
        }
    };
    throw new TypeError(s ? "Object is not iterable." : "Symbol.iterator is not defined.");
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
var amqplib_1 = __importDefault(require("amqplib"));
/**
 * Exchange type 列挙型
 */
var ExchangeType;
(function (ExchangeType) {
    ExchangeType["fanout"] = "fanout";
    ExchangeType["direct"] = "direct";
    ExchangeType["topic"] = "topic";
})(ExchangeType = exports.ExchangeType || (exports.ExchangeType = {}));
/**
 * AMQP 基底クラス
 * AMQPへ接続するクラスの基底クラス
 * ----------------------------------------------------------------------------
 * @author LIS
 */
var RmAmqpBase = /** @class */ (function () {
    /**
     * コンストラクタ
     * @param conf 接続設定
     */
    function RmAmqpBase(conf) {
        this.uri = "";
        this.connected = false;
        this.conn = null;
        this.uri = "amqp://" + conf.user + ":" + conf.password + "@" + conf.host + ":" + conf.port;
    }
    /**
     * 接続処理
     */
    RmAmqpBase.prototype.connect = function () {
        return __awaiter(this, void 0, void 0, function () {
            var _a;
            return __generator(this, function (_b) {
                switch (_b.label) {
                    case 0:
                        if (!(this.connected == false)) return [3 /*break*/, 2];
                        _a = this;
                        return [4 /*yield*/, amqplib_1.default.connect(this.uri)];
                    case 1:
                        _a.conn = _b.sent();
                        this.connected = true;
                        _b.label = 2;
                    case 2: return [2 /*return*/, Promise.resolve(this.conn)];
                }
            });
        });
    };
    /**
     * チャネル作成
     */
    RmAmqpBase.prototype.createChannel = function () {
        return __awaiter(this, void 0, void 0, function () {
            var ch;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        if (this.connected == false) {
                            throw "接続されていないので、チャネルは作成できません。";
                        }
                        return [4 /*yield*/, this.conn.createChannel()];
                    case 1:
                        ch = _a.sent();
                        return [2 /*return*/, Promise.resolve(ch)];
                }
            });
        });
    };
    /**
     * 切断処理
     * ５秒後に切断する
     */
    RmAmqpBase.prototype.close = function () {
        var _this = this;
        if (!this.connected)
            return;
        setTimeout(function (conn) {
            conn.close();
            _this.connected = false;
        }, 500, this.conn);
    };
    return RmAmqpBase;
}());
exports.RmAmqpBase = RmAmqpBase;
/**
 * AMQP Producerクラス
 * AMQPのProduserを生成し、メッセージ送信を可能とする。
 * ----------------------------------------------------------------------------
 * @author LIS
 */
var RmAmqpProducer = /** @class */ (function (_super) {
    __extends(RmAmqpProducer, _super);
    /**
     * コンストラクタ
     * @param conf 接続設定
     */
    function RmAmqpProducer(conf) {
        return _super.call(this, conf) || this;
    }
    /**
     * AMQPメッセージ送信（文字列メッセージ）
     * @param ch Channel
     * @param exName Exchange名
     * @param exType Exchangeタイプ
     * @param msg 送信メッセージ
     */
    RmAmqpProducer.prototype.publishMessage = function (ch, exName, exType, msg) {
        return __awaiter(this, void 0, void 0, function () {
            var datas, data;
            return __generator(this, function (_a) {
                datas = [];
                data = {
                    data: Buffer.from(msg.msg),
                    bindKey: msg.bindKey
                };
                datas.push(data);
                return [2 /*return*/, this.publishBinarys(ch, exName, exType, datas)];
            });
        });
    };
    /**
     * AMQP複数メッセージ送信（文字列メッセージ）
     * @param ch Channel
     * @param exName Exchange名
     * @param exType Exchangeタイプ
     * @param msgs 送信メッセージ配列
     */
    RmAmqpProducer.prototype.publishMessages = function (ch, exName, exType, msgs) {
        return __awaiter(this, void 0, void 0, function () {
            var datas;
            return __generator(this, function (_a) {
                datas = [];
                msgs.forEach(function (msg) {
                    var data = {
                        data: Buffer.from(msg.msg),
                        bindKey: msg.bindKey
                    };
                    datas.push(data);
                });
                return [2 /*return*/, this.publishBinarys(ch, exName, exType, datas)];
            });
        });
    };
    /**
     * AMQPデータ送信（バイナリメッセージ）
     * @param ch Channel
     * @param exName Exchange名
     * @param exType Exchangeタイプ
     * @param data 送信データ
     */
    RmAmqpProducer.prototype.publishBinary = function (ch, exName, exType, data) {
        return __awaiter(this, void 0, void 0, function () {
            var datas;
            return __generator(this, function (_a) {
                datas = [];
                datas.push(data);
                return [2 /*return*/, this.publishBinarys(ch, exName, exType, datas)];
            });
        });
    };
    /**
     * AMQP複数データ送信（バイナリメッセージ）
     * @param ch Channel
     * @param exName Exchange名
     * @param exType Exchangeタイプ
     * @param datas 送信データ配列
     */
    RmAmqpProducer.prototype.publishBinarys = function (ch, exName, exType, datas) {
        return __awaiter(this, void 0, void 0, function () {
            var datas_1, datas_1_1, data;
            var e_1, _a;
            return __generator(this, function (_b) {
                //Exchange差k末井
                ch.assertExchange(exName, exType.toString(), { durable: true });
                try {
                    //Exchangeにバインドキーを指定たメッセージを送信させる
                    for (datas_1 = __values(datas), datas_1_1 = datas_1.next(); !datas_1_1.done; datas_1_1 = datas_1.next()) {
                        data = datas_1_1.value;
                        ch.publish(exName, data.bindKey, data.data);
                    }
                }
                catch (e_1_1) { e_1 = { error: e_1_1 }; }
                finally {
                    try {
                        if (datas_1_1 && !datas_1_1.done && (_a = datas_1.return)) _a.call(datas_1);
                    }
                    finally { if (e_1) throw e_1.error; }
                }
                return [2 /*return*/, Promise.resolve(ch)];
            });
        });
    };
    return RmAmqpProducer;
}(RmAmqpBase));
exports.RmAmqpProducer = RmAmqpProducer;
/**
 * AMQP Consumerクラス
 * AMQPのConsumerを生成し、メッセージ送信を可能とする。
 * ----------------------------------------------------------------------------
 * @author LIS
 */
var RmAmqpConsumer = /** @class */ (function (_super) {
    __extends(RmAmqpConsumer, _super);
    /**
     * コンストラクタ
     * @param conf 接続設定
     */
    function RmAmqpConsumer(conf) {
        return _super.call(this, conf) || this;
    }
    /**
     * コンシューマ実行
     * @param exName Exchange名
     * @param exType Exchengeタイプ
     * @param queueName キュー名
     * @param bindKeys Exchange and Queueバインドキー
     * @param func メッセージ受信処理
     */
    RmAmqpConsumer.prototype.consume = function (ch, exName, exType, queueName, bindKeys, 
    // eslint-disable-next-line @typescript-eslint/no-explicit-any
    consumeFunc) {
        return __awaiter(this, void 0, void 0, function () {
            var q, conOption;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        //Exchange作成
                        ch.assertExchange(exName, exType.toString(), { durable: true });
                        return [4 /*yield*/, ch.assertQueue(queueName, { exclusive: true })];
                    case 1:
                        q = _a.sent();
                        //ExchangeとQueueの関係を設定
                        bindKeys.forEach(function (key) { return ch.bindQueue(q.queue, exName, key); });
                        conOption = {
                            noAck: false
                        };
                        //受信処理登録
                        ch.consume(q.queue, consumeFunc, conOption);
                        return [2 /*return*/, Promise.resolve(ch)];
                }
            });
        });
    };
    return RmAmqpConsumer;
}(RmAmqpBase));
exports.RmAmqpConsumer = RmAmqpConsumer;
//# sourceMappingURL=rm-amqp.js.map