/// <reference types="node" />
import amqp from "amqplib";
/**
 * Exchange type 列挙型
 */
export declare enum ExchangeType {
    fanout = "fanout",
    direct = "direct",
    topic = "topic"
}
/**
 * AMQP接続設定型
 */
export interface AmqpConnectConfig {
    host: string;
    port: number;
    user: string;
    password: string;
    vhost: string;
}
/**
 * AMQP送信メッセージ（文字列）型
 */
export interface AmqpSendMsg {
    msg: string;
    bindKey: string;
}
/**
 * AMQP送信メッセージ（バイナリ）型
 */
export interface AmqpSendData {
    data: Buffer;
    bindKey: string;
}
/**
 * AMQP 基底クラス
 * AMQPへ接続するクラスの基底クラス
 * ----------------------------------------------------------------------------
 * @author LIS
 */
export declare class RmAmqpBase {
    uri: string;
    connected: boolean;
    conn: amqp.Connection;
    /**
     * コンストラクタ
     * @param conf 接続設定
     */
    constructor(conf: AmqpConnectConfig);
    /**
     * 接続処理
     */
    connect(): Promise<amqp.Connection>;
    /**
     * チャネル作成
     */
    createChannel(): Promise<amqp.Channel>;
    /**
     * 切断処理
     * ５秒後に切断する
     */
    close(): void;
}
/**
 * AMQP Producerクラス
 * AMQPのProduserを生成し、メッセージ送信を可能とする。
 * ----------------------------------------------------------------------------
 * @author LIS
 */
export declare class RmAmqpProducer extends RmAmqpBase {
    /**
     * コンストラクタ
     * @param conf 接続設定
     */
    constructor(conf: AmqpConnectConfig);
    /**
     * AMQPメッセージ送信（文字列メッセージ）
     * @param ch Channel
     * @param exName Exchange名
     * @param exType Exchangeタイプ
     * @param msg 送信メッセージ
     */
    publishMessage(ch: amqp.Channel, exName: string, exType: ExchangeType, msg: AmqpSendMsg): Promise<amqp.Channel>;
    /**
     * AMQP複数メッセージ送信（文字列メッセージ）
     * @param ch Channel
     * @param exName Exchange名
     * @param exType Exchangeタイプ
     * @param msgs 送信メッセージ配列
     */
    publishMessages(ch: amqp.Channel, exName: string, exType: ExchangeType, msgs: AmqpSendMsg[]): Promise<amqp.Channel>;
    /**
     * AMQPデータ送信（バイナリメッセージ）
     * @param ch Channel
     * @param exName Exchange名
     * @param exType Exchangeタイプ
     * @param data 送信データ
     */
    publishBinary(ch: amqp.Channel, exName: string, exType: ExchangeType, data: AmqpSendData): Promise<amqp.Channel>;
    /**
     * AMQP複数データ送信（バイナリメッセージ）
     * @param ch Channel
     * @param exName Exchange名
     * @param exType Exchangeタイプ
     * @param datas 送信データ配列
     */
    publishBinarys(ch: amqp.Channel, exName: string, exType: ExchangeType, datas: AmqpSendData[]): Promise<amqp.Channel>;
}
/**
 * AMQP Consumerクラス
 * AMQPのConsumerを生成し、メッセージ送信を可能とする。
 * ----------------------------------------------------------------------------
 * @author LIS
 */
export declare class RmAmqpConsumer extends RmAmqpBase {
    /**
     * コンストラクタ
     * @param conf 接続設定
     */
    constructor(conf: AmqpConnectConfig);
    /**
     * コンシューマ実行
     * @param exName Exchange名
     * @param exType Exchengeタイプ
     * @param queueName キュー名
     * @param bindKeys Exchange and Queueバインドキー
     * @param func メッセージ受信処理
     */
    consume(ch: amqp.Channel, exName: string, exType: ExchangeType, queueName: string, bindKeys: string[], consumeFunc: (msg: amqp.ConsumeMessage) => any): Promise<amqp.Channel>;
}
//# sourceMappingURL=rm-amqp.d.ts.map